package pages;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;

import org.sikuli.script.FindFailed;

import org.sikuli.script.Key;

public class AsignacionCuotaPage extends BasePage {
	
	

	public String base__fechaActual() {
		Calendar hoy = Calendar.getInstance();
		//hoy.add(Calendar.DATE, 3);
		//hoy.add(Calendar.MONTH, 2);
		SimpleDateFormat format1 = new SimpleDateFormat("dd/MM/yyyy");
		String formatted = format1.format(hoy.getTime());
		//System.out.println(hoy.getTime());
		return (formatted);
	}
	
	 public void VolverMenu() {
		 
		 
		 try {
			click("CuotaSalir");
		} catch (FindFailed e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		 
	 }
	 
	 

	 public void ProgramacionCuotaPrev(String fila) throws FindFailed {
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		 LocalDate localDate = LocalDate.now();
		 String fechaAct;
		 System.out.println(dtf.format(localDate)); //2016/11/16
	/*	
	 	//FIXME VER COMO OBTENER EL DATO CON CAPTURA DE IMAGEN.
		 String nombre = "Solicitud"+fila;
		 NumeroDeSolicitud = GetTextFromImg("NroSolicitud", "NroSolicitud2",nombre);
		 System.out.println("Solicitud : "+NumeroDeSolicitud);
	*/
		 String ogese = dameDatoCuota("AE"+fila).toLowerCase();
		 String ejercicio = dameDatoCuota("AD"+fila).toLowerCase();
		 String porcentaje = dameDatoCuota("AG"+fila).toLowerCase();
		 // String NroCuota ;
		 
		 espera2();
		 click("MenuPresupuesto");
		 espera();
		 dclick("MenuProgramacion");
		 espera();
		 dclick("MenuProgCuotaPrev");
		 clickfocustype("PreventivoEjercicio", 21, 3, ejercicio);
		 clickfocustype("PreventivoOgese", 16, 0, ogese);
		 type(Key.F10);
		 click("GuardarSI");
		 click("AceptarGuardar");
		 espera();
		 type(Key.F7);
		 espera();
		 type("6727"); //FIXME cambiar esto despues, esta hardcodeado.
		 espera();
		 type(Key.F8);
		 espera();
		 dclickfocustype("PreventivoPorcentaje", 14, 12, Key.BACKSPACE);
		 espera();
		 dclickfocustype("PreventivoPorcentaje", 14, 12, porcentaje);
		 espera();
		 //type(porcentaje);
		 type(Key.TAB);
		 type(Key.F10);
		 click("GuardarSI");
		 click("AceptarGuardar");
		 click("PreventivoDarCierre");
		 click("AceptarGuardar");
		 click("AceptarGuardar");
	 }
	 
public void ReProgramacionCuota(String fila) throws FindFailed {
		 
		 DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
		 LocalDate localDate = LocalDate.now();
		 String fechaAct;
		 System.out.println(dtf.format(localDate)); //2016/11/16
	/*	
	 	//FIXME VER COMO OBTENER EL DATO CON CAPTURA DE IMAGEN.
		 String nombre = "Solicitud"+fila;
		 NumeroDeSolicitud = GetTextFromImg("NroSolicitud", "NroSolicitud2",nombre);
		 System.out.println("Solicitud : "+NumeroDeSolicitud);
	*/
		 String ogese = dameDatoCuota("AE"+fila).toLowerCase();
		 String ejercicio = dameDatoCuota("AD"+fila).toLowerCase();
		 String porcentaje1 = dameDatoCuota("AH"+fila).toLowerCase();
		 String porcentaje2 = dameDatoCuota("AI"+fila).toLowerCase();
		 String porcentaje3 = dameDatoCuota("AJ"+fila).toLowerCase();
		 String porcentaje4 = dameDatoCuota("AK"+fila).toLowerCase();
		 String NroCuota = "6727"; //FIXME este dato hay que sacarlo del paso anterior, ahora esta Hardcode.
		 
		 
		 //FIXME empieza ya con los menus abierto excepto el ultimo
		 espera2();
		 click("MenuPresupuesto");
		 espera();
		 dclick("MenuProgramacion");
		 espera();
		 dclick("MenuReprogramacionCuota");
		 espera();
		 clickfocustype("ReprogEjercicio", 21, 5, ejercicio);
		 clickfocustype("ReprogOgese", 16, 0, ogese);
		 type(Key.F10);
		 click("GuardarSI");
		 click("AceptarGuardar");
		 click("AceptarGuardar");
		 dclickfocustype("ReprogCuota", 7, 17, Key.F7);
		 espera();
		 type(NroCuota);
		 type(Key.F8);
		 dclickfocustype("ReprogTrim1", 25, 20, porcentaje1 + "%");
		 type(Key.TAB);
		 type(porcentaje2 + "%");
		 type(Key.TAB);
		 type(porcentaje3 + "%");
		 type(Key.TAB);
		 type(porcentaje4 + "%");
		 espera();
		 clickfocus("ReprogNormAprobTipo", -4, 16);
		 type(Key.F10);
		 click("GuardarSI");
		 click("AceptarGuardar");
		 click("ReprogAprobarRevision");
		 click("ReprogDarCierre");
		 click("GuardarSI");
		 click("ReprogAceptar");
		 
		 
		 
	 }

	public void DistribucionCuota(String fila) throws FindFailed {
		String ogese = dameDatoCuota("AE"+fila).toLowerCase();
		String ejercicio = dameDatoCuota("AD"+fila).toLowerCase();
		String porcentajeTrimestral1 = dameDatoCuota("AL"+fila).toLowerCase();
		String porcentajeTrimestral2 = dameDatoCuota("AM"+fila).toLowerCase();
		String porcentajeTrimestral3 = dameDatoCuota("AN"+fila).toLowerCase();
		String porcentajeTrimestral4 = dameDatoCuota("AO"+fila).toLowerCase();
		String NroCuota = "6727"; //FIXME este dato hay que sacarlo del paso anterior, ahora esta Hardcode.
		
		espera2();
		click("MenuPresupuesto");
		espera();
		dclick("MenuProgramacion");
		espera();
		dclick("MenuDistribucionCuotas");
		espera();
		dclickfocustype("DistEjercicio", 23, 0, ejercicio);
		clickfocustype("DistOgese", 8, 0, ogese);
		clickfocustype("DistCuotaOtorgada", 5, 2, NroCuota);
		type(Key.F8);
		wait("DistFindPrograma");
		clickfocus("DistFindPrograma", 200, 0);
		type(porcentajeTrimestral1);
		type(Key.TAB);
		type(Key.TAB);
		type(porcentajeTrimestral2);
		type(Key.TAB);
		type(Key.TAB);
		type(porcentajeTrimestral3);
		type(Key.TAB);
		type(Key.TAB);
		type(porcentajeTrimestral4);
		type(Key.F10);
		click("GuardarSI");
		click("AceptarGuardar");

	}
	
}
