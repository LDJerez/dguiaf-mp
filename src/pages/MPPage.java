package pages;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Key;

public class MPPage extends BasePage{

	public void DatosDeMP(String fila) throws FindFailed {
	String ejercicio=dameDato("D"+fila);
	String ajuste = dameDato("E"+fila);
	String origenact = dameDato("F"+fila);
	String tipoact = dameDato("G"+fila);
	String normaapro = dameDato("H"+fila);
	String motivo = dameDato("I"+fila);
	String nrocamp1= dameDato("J"+fila);
	String nrocamp2= dameDato("K"+fila);
	String nrocamp3= dameDato("L"+fila);
	String nrocamp4= dameDato("M"+fila);
	espera();
	clickfocustype("IngresoMPEjercicio", 60,ejercicio );
	clickfocustype("MPTipodeAjuste", 60,ajuste );
	clickfocustype("MPActOrigen", 60,origenact );
	dclickfocustype("MPMotivo", 15,motivo );
	clickfocustype("MPActTipo", 60,tipoact );
	clickfocustype("MPNormaAprob", 60,normaapro );
	click("NroCampo1");
	for(int i = 0; i <3;i++){
	espera();
	type(nrocamp1);
	type(Key.TAB);
	espera();
	type(nrocamp2);
	type(Key.TAB);
	espera();
	type(nrocamp3);
	type(Key.TAB);
	espera();
	type(nrocamp4);
	type(Key.TAB);
	type(Key.TAB);
	}}
	
public void Observaciones(String fila) throws FindFailed {
	String ObsSector = dameDato("N"+fila);
	String ObsNro = dameDato("O"+fila);
	String ObsTxt = dameDato("P"+fila);
	clickfocus("MPActInt", 0, 15);
	click("AceptarGuardar");
	clickandtype("ObsSector", ObsSector);
	clickandtype("ObsNro", ObsNro);
	clickandtype("ObsTxt", ObsTxt);
	click("ObsVolver");
	click("GuardarSI");
	espera();
	click("AceptarGuardar");
}
public void DatosGrilla(String fila)throws FindFailed{
	String MPActInt = dameDato("Q"+fila);
	String MPInciso = dameDato("R"+fila);
	String MPPpal = dameDato("S"+fila);
	String MPPar = dameDato("T"+fila);
	String MPPSpar = dameDato("U"+fila);
	String MPFF = dameDato("V"+fila);
	String MPMoneda = dameDato("W"+fila);
	String MPUG = dameDato("X"+fila);
	String MPImporte = dameDato("Y"+fila);
	String MPExeptuar = dameDato("Z"+fila);
	clickfocustype("MPActInt", 0, 15,MPActInt);
	clickfocustype("MPInciso", 0, 15,MPInciso);
	clickfocustype("MPPpal", 0, 15,MPPpal);
	clickfocustype("MPPar", 0, 15,MPPar);
	clickfocustype("MPPSpar", 0, 15,MPPSpar);
	clickfocustype("MPFF", 0, 15,MPFF);
	clickfocustype("MPMoneda", 0, 15,MPMoneda);
	clickfocustype("MPUG", 0, 15,MPUG);
	clickfocustype("MPImporte", 0, 15,MPImporte);
	if(MPExeptuar.toLowerCase().equals("si")) {
	click("MPExeptuarSI");}
}
	public void PendienteFisica() throws FindFailed {
		type(Key.F10);
		click("GuardarSI");
		click("AceptarGuardar");
		click("MPPendienteFisica");
		click("AceptarGuardar");
		click("AceptarGuardar");
	}
	
	public void BuscarRequerimiento(String fila,String NroReq) throws FindFailed {
		String ejercicio=dameDato("D"+fila);
		type(Key.F7);
		clickfocustype("IngresoMPEjercicio", 60,ejercicio );
		clickfocustype("BscrReq", 20, NroReq);
		click("btnConsulta");
	}
	public void IngresarPendiente() throws FindFailed {
		click("btnPendiente");
		click("AceptarGuardar");
		click("AceptarGuardar");
	}
	
	public void IngresarAprobar() throws FindFailed {
		click("btnAprobar");
		click("AceptarGuardar");
		click("AceptarGuardar");
	}
}
