package pages;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;


import javax.imageio.ImageIO;

import org.sikuli.script.FindFailed;

public class EditorDeImagen extends BasePage {
	
	  /**
     * Resizes an image to a absolute width and height (the image may not be
     * proportional)
     * @param inputImagePath Path of the original image
     * @param outputImagePath Path to save the resized image
     * @param scaledWidth absolute width in pixels
     * @param scaledHeight absolute height in pixels
     * @throws IOException
     */
	    public static void resize(String inputImagePath,
	            String outputImagePath, int scaledWidth, int scaledHeight)
	            throws IOException {
	        // reads input image
	        File inputFile = new File(inputImagePath);
	        BufferedImage inputImage = ImageIO.read(inputFile);
	 
	        // creates output image
	        BufferedImage outputImage = new BufferedImage(scaledWidth,
	                scaledHeight, inputImage.getType());
	 
	        // scales the input image to the output image
	        Graphics2D g2d = outputImage.createGraphics();
	        g2d.drawImage(inputImage, 0, 0, scaledWidth, scaledHeight, null);
	        g2d.dispose();
	 
	        // extracts extension of output file
	        String formatName = outputImagePath.substring(outputImagePath
	                .lastIndexOf(".") + 1);
	 
	        // writes to output file
	        ImageIO.write(outputImage, formatName, new File(outputImagePath));
	    }
	 
	    /**
	     * Resizes an image by a percentage of original size (proportional).
	     * @param inputImagePath Path of the original image
	     * @param outputImagePath Path to save the resized image
	     * @param percent a double number specifies percentage of the output image
	     * over the input image.
	     * @throws IOException
	     */
	    public static void resize(String inputImagePath,
	            String outputImagePath, double percent) throws IOException {
	        File inputFile = new File(inputImagePath);
	        BufferedImage inputImage = ImageIO.read(inputFile);
	        int scaledWidth = (int) (inputImage.getWidth() * percent);
	        int scaledHeight = (int) (inputImage.getHeight() * percent);
	        resize(inputImagePath, outputImagePath, scaledWidth, scaledHeight);
	    }


	public void editimage(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
	String inputImagePath = imgtemporaria(nombreimagen, nombreimagen2);
	String outputImagePath = ".\\images.sikuli\\numeros\\"+nombre+".png";


    try { double percent = 3;
        resize(inputImagePath, outputImagePath, percent);
    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
	  try {	Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
	  espera2();
	}
	
	public void editimage2(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
	String inputImagePath = imgtemporaria2(nombreimagen, nombreimagen2);
	String outputImagePath = ".\\images.sikuli\\numeros\\"+nombre+".png";

    try { double percent = 3;
        resize(inputImagePath, outputImagePath, percent);
    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
	  try {	Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
	  espera2();
	}
	
	  public void editimage3(String nombreimagen, String nombreimagen2,String nombre) throws IOException, FindFailed {
		  String inputImagePath = imgtemporaria3(nombreimagen, nombreimagen2);
		  String outputImagePath = ".\\images.sikuli\\numeros\\"+nombre+".png";

		    try { double percent = 3;
		        resize(inputImagePath, outputImagePath, percent);
		    } catch (IOException ex) {System.out.println("Error resizing the image.");ex.printStackTrace();}
		    try {  Thread.sleep(5000);} catch (InterruptedException e) {e.printStackTrace();}
		    espera2();
}
}
