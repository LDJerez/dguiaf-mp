package pages;

import sikulix.OCR;
import sikulix.util.ImageFrame;
import org.sikuli.script.FindFailed;
import org.sikuli.script.Location;
import org.sikuli.script.Match;
import org.sikuli.script.Region;
import org.sikuli.script.Screen;

import pages.ManipularExcel;
import sigaf.ManipularExcelCuotas;

import java.io.IOException;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class BasePage{
	public static Screen sBP = null;
	String FilasTotales; 
	String repositorio = "./images.sikuli/";
	String carpetaNumeros= repositorio +"numeros/";

	public static String dameDato(String d) {
		ManipularExcel.setArchivoExcel(ManipularExcel.getArchivoExcel());
		return ManipularExcel.obtenerDatoDeCelda(ManipularExcel.getArchivoExcel(), d);
	}
	
	  //FIXME Esto lo hice para tener datos en cuotas - Arturo
		public static String dameDatoCuota(String d) {
			ManipularExcelCuotas.setArchivoExcel(ManipularExcelCuotas.getArchivoExcel());
			return ManipularExcelCuotas.obtenerDatoDeCelda(ManipularExcelCuotas.getArchivoExcel(), d);
		}
	   Screen  s  =  new  Screen ();
    private static OCR ocr = null;   
    public void SelectRegionyClick(String imagenregion, String imagenclick) throws FindFailed {
    espera();
 	   Match f = s.find(img(imagenregion)).find(img(imagenclick)); 	   	
 	   f.click();
 }
    

    public void SelectRegionyClickAll(String imagenregion, String imagenclick) throws FindFailed{
  	   Match f = s.find(img(imagenregion));
  	 Iterator<Match> r = f.findAll(img(imagenclick));
  	 r.next().click();
    }
 public void SelectRegionyDClick(String imagenregion, String imagenclick) throws FindFailed {
	 s.wait(img(imagenregion),10);
 	   Match f = s.find(img(imagenregion));
 	  espera();
 	   Region r=f.find(img(imagenclick)) ;
 	  espera();
 	   r.doubleClick();	
 }

    public String img(String nombreimagen) {
    	String imagen = repositorio+nombreimagen+".png";
    	return imagen;}
    
    
    
    
   	public void Login (String perfil)throws InterruptedException, FindFailed  {
   	clickandtype("UsuarioLogin",dameDato("A3").toLowerCase());
   	clickandtype("PswdLogin",dameDato("B3").toLowerCase());
   	click("PerfilLogin");
   	if(perfil.contains(dameDato("C3").toLowerCase())) {
   	click("PerfilOPerez");
   	}else {
   	click("OrganoRector");}
   	click("AceptarLogin");
   	}
	
	public void deslogin() throws FindFailed {
		boolean a ;
		for(int i=0; i<10; i++) {
			a = verificarimagen("desconectar");
			System.out.println(a);
			if(a=true){
				break;}
		try {click("GuardarNo");} catch (NoSuchElementException | FindFailed e) {}	
		espera();
		try {click("AceptarGuardar");} catch (NoSuchElementException | FindFailed e) {}	
		espera();
		try {volvermenu();} catch (NoSuchElementException | FindFailed e) {}	
		}
		click("desconectar");
	}

	//FIXME Esto lo hice para loggear en cuotas - Arturo
		public void LoginCuota (String fila)throws InterruptedException, FindFailed  {
			String perfil = "Juri. Nro 60 - DGTAyL Nro. 60 - Uoc. Nro 608";
		clickandtype("UsuarioLogin",dameDatoCuota("A"+fila).toLowerCase());
		clickandtype("PswdLogin",dameDatoCuota("B"+fila).toLowerCase());
		click("PerfilLogin");
		if(perfil.contains(dameDatoCuota("C3").toLowerCase())) {
		click("OrganoRector");
		}else {
		click("OrganoRector");}
		click("AceptarLogin");
		}
	
/////acciones/////
	public void wait(String nombreimagen) throws FindFailed {
		s.wait(img(nombreimagen), 10);	}
	
	public void click(String nombreimagen) throws FindFailed {
		espera();
			s.wait(img(nombreimagen), 10);
			espera();
		s.click(img(nombreimagen));
		espera();}
	
	public void find(String nombreimagen) throws FindFailed {
		s.wait(img(nombreimagen),10);
		s.find(img(nombreimagen));}

	public void dclick(String nombreimagen) throws FindFailed {
    	s.wait(img(nombreimagen), 10);
    	s.doubleClick(img(nombreimagen));}

	public void type(String txt) {
		espera();
		s.type(txt);
		espera();
	}
	
	public void clickandtype(String nombreimagen,String txt) throws FindFailed {
		espera();
		s.type(img(nombreimagen), txt);
	}
	
	public void dclickfocustype(String nombreimagen, int x, String txt) throws FindFailed {
		Match imagen = s.find(img(nombreimagen));
		imagen.setTargetOffset(x,0);
		imagen.doubleClick();
		espera();
		type(txt);
	} 
	public void clickfocustype(String nombreimagen, int x, String txt) throws FindFailed {
		espera();
		Match imagen = s.find(img(nombreimagen));
		imagen.setTargetOffset(x,0);
		imagen.click();
		espera();
		type(txt);


	}
	public void clickfocustype(String nombreimagen, int x, int y, String txt) throws FindFailed {
		espera();
		Match imagen = s.find(img(nombreimagen));
		espera();
		imagen.setTargetOffset(x,y);
		imagen.click();
		espera();
		type(txt);
	}

public void dclickfocustype(String nombreimagen, int x, int y, String txt) throws FindFailed {
	espera();
	Match imagen = s.find(img(nombreimagen));
	imagen.setTargetOffset(x,y);
	imagen.doubleClick();
	espera();
	type(txt);
}
	public void clickfocus(String nombreimagen, int x) throws FindFailed {
		espera();
		Match imagen = s.find(img(nombreimagen));
		imagen.setTargetOffset(x,0);
		imagen.click();
		espera();		
	}
	
	public void clickfocus(String nombreimagen, int x, int y) throws FindFailed {
		espera();
		Match imagen = s.find(img(nombreimagen));
		imagen.setTargetOffset(x,y);
		espera();
		imagen.click();
		espera();
	}
	public void dclickfocus(String nombreimagen, int x, int y) throws FindFailed {
		espera();
		Match imagen = s.find(img(nombreimagen));
		imagen.setTargetOffset(x,y);
		imagen.doubleClick();
		espera();
	}
	
	public static void espera() {
		try {Thread.sleep(500);} catch (InterruptedException e) {
			e.printStackTrace();}}
	
	public void espera2() {
		try {Thread.sleep(5000);} catch (InterruptedException e) {
			e.printStackTrace();}}
	

	public void volvermenu() throws FindFailed {
		dclick("volvermenu");
	}
/////Ingresos/////
	public void IngresoMP() throws FindFailed {
		click("MenuPresupuesto");
		dclick("MenuModificaciones");
		dclick("MenuIngresoYModif");
	}
	public void IngresoMP2() throws FindFailed {
		dclick("MenuIngresoYModif2");
	}
//***************************************************************************************************+++
//////Metodos///////
	
	public void SacarFoto(String nombreimagen,String nombreimagen2,String nombre) throws FindFailed, IOException {
		EditorDeImagen EDI = new EditorDeImagen();
		  
		 EDI.editimage2(nombreimagen, nombreimagen2,nombre);
		  String outputImagePath = carpetaNumeros+nombre+".png";
		@SuppressWarnings("unused")
		ImageFrame f = new ImageFrame(outputImagePath);
	}
	public String GetTextFromImg(String nombreimagen,String nombreimagen2,String nombre) throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();		  
	 EDI.editimage(nombreimagen, nombreimagen2,nombre);		
		String outputImagePath = carpetaNumeros+nombre+".png";
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		f.close();
		
		return text;
	}
	

	public String GetTextFromImg2(String nombreimagen,String nombreimagen2,String nombre ) throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();
		  
		 EDI.editimage2(nombreimagen, nombreimagen2,nombre);
			espera2();

			String outputImagePath = carpetaNumeros+nombre+".png";
			  
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		espera();
		f.close();
		return text;
	}
	
	public String GetTextFromImg3(String nombreimagen,String nombreimagen2,String nombre) throws FindFailed, IOException {
		OCR.setStoragePath("./resources/glyphs");
		ocr = OCR.getSpec("numbers");
		EditorDeImagen EDI = new EditorDeImagen();
		String outputImagePath = carpetaNumeros +nombre+".png";
		 EDI.editimage3(nombreimagen, nombreimagen2,nombre);
			espera2();
		ImageFrame f = new ImageFrame(outputImagePath);
		String text = ocr.read(f.getBounds());
		espera();
		f.close();
		return text;
	}
	public String imgtemporaria(String nombreimagen,String nombreimagen2) throws FindFailed {
		   Match f = s.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getTopRight();
		   @SuppressWarnings("deprecation")
		String txt =  f.moveTo(b).saveScreenCapture();
		return txt;
		}
	

	
	public String imgtemporaria2(String nombreimagen,String nombreimagen2) throws FindFailed {
		   Match f = s.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getBottomLeft();
		   @SuppressWarnings("deprecation")
		String txt =  f.moveTo(b).saveScreenCapture();
		return txt;
		}
	
	public String imgtemporaria3(String nombreimagen,String nombreimagen2) throws FindFailed {
		   Match f = s.find(img(nombreimagen)).find(img(nombreimagen2));
		Location b = f.getTopRight();
		   @SuppressWarnings("deprecation")
		String txt =  f.moveTo(b).saveScreenCapture();
		return txt;
		} 
	
	public boolean verificarimagen(String nombreimagen){
		boolean a = true;
		try {boolean b= s.find(img(nombreimagen)).isValid();
		System.out.println(b);
		} catch (NoSuchElementException | FindFailed e) {a = false;}
		return a;}
	


//*************************************************

}	
	
	
