package sigaf;

import java.io.IOException;
import java.util.ArrayList;

import org.sikuli.script.FindFailed;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import pages.BasePage;
import pages.MPPage;
import pages.ManipularExcel;
import sikulix.Splash;

public class ModificacionPresupuestaria extends BasePage{
	int Loop = ManipularExcel.MAXIMO_FILAS;
	
	//String fila = "3";
	String NroReq;
	 @DataProvider(name = "filas")
	    private Object[] createData1() {
		 ArrayList<String> filas = new ArrayList<>();
			for(int i=3; i<=Loop; i++) {
				String C= String.valueOf(i);
				filas.add(C);
			}
		       return filas.toArray();
	    }
	 
	 @BeforeClass
		public void pantallaSplash() {
			Splash sp = null;
			String repositorio = "./images.sikuli/";
		  	sp = new Splash(null,"Modificacion Presupuestaria",repositorio+"Splash.png");
			sp.show();
			try { Thread.sleep(3000);} catch (InterruptedException e) {	e.printStackTrace();}
			sp.dispose();
		}
		
	 @Parameters({"filas"}) 
	 @BeforeMethod
	 public void IngresoAPP(String fila) throws FindFailed, InterruptedException {
		
		LoginCuota(fila);
	}
	 
	 @AfterMethod
	 public void salirAPP() throws FindFailed{
		 System.out.println("Requerimiento creado con el numero: "+NroReq);
		 deslogin();
	 }
	 
	 
	@Test(dataProvider = "filas")
	 public void Flujo_Modificacion_Presupuestaria(String fila) throws FindFailed, IOException{
		 TS01_Modificacion_de_Presupuesto_Inicio(fila);
		 TS02_Modificacion_de_Presupuesto_Pendiente(fila);
		 TS03_Inicio_Modificacion_de_Presupuesto_Aprobacion(fila);
		 AsignacionCuota AC = new AsignacionCuota();
		 AC.TS01_PreventivoCuotas(fila);
		 AC.TS02_ReProgramacionPreventivoCuotas(fila);
		 AC.TS03_DistribucionDeCuotas(fila);
		 
	 }

	//		 @Test
			 public void TS01_Modificacion_de_Presupuesto_Inicio(String fila) throws FindFailed, IOException {
				 String nombre = "NroReq";
				 IngresoMP();
				 MPPage MPP = new MPPage();
				 MPP.DatosDeMP(fila);
				 MPP.Observaciones(fila);
				 MPP.DatosGrilla(fila);
				 MPP.PendienteFisica();
				 NroReq = GetTextFromImg("NroReq", "NroReq", nombre+fila);
				 volvermenu();
			 }
			 
	//		 @Test
			 public void TS02_Modificacion_de_Presupuesto_Pendiente(String fila) throws FindFailed {
				 IngresoMP2();
				 MPPage MPP = new MPPage();
				 MPP.BuscarRequerimiento(fila, NroReq);
				 MPP.IngresarPendiente();
				 volvermenu();
			 }
			 
//			 @Test
			 public void TS03_Inicio_Modificacion_de_Presupuesto_Aprobacion(String fila) throws FindFailed {
				 IngresoMP2();
				 MPPage MPP = new MPPage();
				 MPP.BuscarRequerimiento(fila, NroReq);
				 MPP.IngresarAprobar();
				 volvermenu();
			 }
			 
			 
			 
	
	
	}