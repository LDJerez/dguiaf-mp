package sigaf;


import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import javax.swing.JOptionPane;
import org.sikuli.script.FindFailed;
import org.testng.TestListenerAdapter;
import org.testng.TestNG;

public class Iniciador {

	@SuppressWarnings("deprecation")
	public static void main(String[] args) throws FindFailed, InterruptedException, IOException, UnsupportedFlavorException {
		
		TestListenerAdapter tla = new TestListenerAdapter();
		TestNG testng = new TestNG();
		testng.setTestClasses(new Class[] { ModificacionPresupuestaria.class });
		testng.addListener(tla);
		testng.run();
		
		  JOptionPane.showMessageDialog(null, "Termino");
		}
	
}
