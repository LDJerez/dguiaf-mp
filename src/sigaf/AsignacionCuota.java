package sigaf;

import java.awt.datatransfer.UnsupportedFlavorException;
import java.io.IOException;
import java.util.ArrayList;

import org.sikuli.script.FindFailed;
import org.sikuli.script.Screen;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import pages.AsignacionCuotaPage;
import pages.BasePage;
import sikulix.Splash;

public class AsignacionCuota extends BasePage{

	
	 @DataProvider(name = "filas")
	    private Object[] createData1() {
		 ArrayList<String> filas = new ArrayList<>();
			for(int i=3; i<=Loop; i++) {
				String C= String.valueOf(i);
				filas.add(C);
			}
		       return filas.toArray();
	    }
	
	String perfil = "Organo Rector";
	int Loop = ManipularExcelCuotas.MAXIMO_FILAS;

/*	String NumeroDeSolicitud;
	String NroParametro;
	String NroOC;
*/
	
  @BeforeMethod
	public void Start () throws InterruptedException, FindFailed  {
	  LoginCuota(perfil.toLowerCase()); 
            	}
	
 @AfterMethod
  public void Desconectar() throws FindFailed {
		
		deslogin();
	}

	
 
 	@Test (dataProvider = "filas")
 	public void TS01_PreventivoCuotas(String fila) throws FindFailed {
 		// System.out.println(ManipularExcelCuotas.MAXIMO_FILAS);
 		AsignacionCuotaPage AC = new AsignacionCuotaPage();
 		AC.ProgramacionCuotaPrev(fila);
 		AC.VolverMenu();
 		
 		}
 	
 	
 	@Test (dataProvider = "filas")
 	public void TS02_ReProgramacionPreventivoCuotas(String fila) throws FindFailed {
 		AsignacionCuotaPage AC = new AsignacionCuotaPage();
 		AC.ReProgramacionCuota(fila);
 		AC.VolverMenu();
 		
 		}
 	
 	@Test (dataProvider = "filas")
 	public void TS03_DistribucionDeCuotas(String fila) throws FindFailed {
 		AsignacionCuotaPage AC = new AsignacionCuotaPage();
 		AC.DistribucionCuota(fila);
 		AC.VolverMenu();
 		
 		}
 	
 	
}
