package sigaf;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellReference;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ManipularExcelCuotas {

	public static String getArchivoExcel() {return archivoExcel;}
	public static void setArchivoExcel(String ae) { archivoExcel = ae;}
	public static String archivoExcel="C:/xcfotech/Modificacion_Presupuestaria.xlsx";
	
	public static int MAXIMO_COLUMNAS = 82, MAXIMO_FILAS = maximofilas(archivoExcel);
		
	/*
	 * Este m�todo imprime el contenido de las celdas de un excel pasado como
	 * par�metro. (excel con extensi�n .XLSX) y la celda desde donde se desea
	 * obtener el dato (en formato ColumnaFila, ejemplo FA3
	 */
	@SuppressWarnings({ "unused", "deprecation" })
	public static String obtenerDatoDeCelda(String archivo,String ubicacionCelda) {	
		
		String filename = archivo;
		File myFile = new File(filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(myFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XSSFWorkbook myWorkBook = null;
		try {
			myWorkBook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);
		String refCelda = "";
		HashMap<String, String> hoja = new HashMap<String, String>();
		int numColumna = 0;
		int numFila = 0;

		// for(Row tmp : mySheet){
		
		for (int f = 0; f < MAXIMO_FILAS; f++) {

			numFila = f + 1;
			Row tmp = mySheet.getRow(f);
			// System.out.println("--------------------- Datos de la fila "+(f+1) +
			// "---------------------");
			for (int col = 0; col < MAXIMO_COLUMNAS; col++) {
				numColumna = col + 1;
				refCelda = CellReference.convertNumToColString(col) + (f + 1);
				if (tmp.getCell(col) == null) {
					String datoNull = "";
					// System.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna
					// "+numColumna+ ") = "+ datoNull );
					hoja.put(refCelda, datoNull);
				} else {
					switch (tmp.getCell(col).getCellType()) {
					case Cell.CELL_TYPE_STRING:
						String datoString = tmp.getCell(col).getStringCellValue();
						//sSystem.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna "+numColumna+ ") = "+ datoString );
						hoja.put(refCelda, datoString);
						break;
					case Cell.CELL_TYPE_NUMERIC:
						int datoInt = (int) tmp.getCell(col).getNumericCellValue();
						// System.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna
						// "+numColumna+ ") = "+ datoInt);
						hoja.put(refCelda, String.valueOf(datoInt));
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						String datoBoolean = "[celda booleana]";
						// System.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna
						// "+numColumna+ ") = "+ datoBoolean );
						hoja.put(refCelda, datoBoolean);
						break;
					case Cell.CELL_TYPE_BLANK:
						String datoBlank = "";
						// System.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna
						// "+numColumna+ ") = "+ datoBlank );
						hoja.put(refCelda, datoBlank);
						break;
					default:
						// System.out.println(" Celda "+refCelda+ " (Fila " + (f+1) + ", Columna
						// "+numColumna+ ") = "+ String.valueOf(tmp.getCell(col)) );
						hoja.put(refCelda, String.valueOf(tmp.getCell(col)));
					}
				}
			} // del for de columnas
		} // del for de filas
			// System.out.println("Muestro celda J2: "+ hoja.get("J2"));
		return hoja.get(ubicacionCelda);
	} // fin del m�todo obtenerDatoDeCelda

	
	/*
	 * Este m�todo devuelve los datos de la fila deseada. El m�todo recibe un excel
	 * con extensi�n .XLSX y el n�mero de fila a obtener.
	 */
	@SuppressWarnings({ "unused", "deprecation" })
	public HashMap<String, String> obtenerFilaExcel(String archivo, int numeroFila) {
		
		String filename = archivo;

		File myFile = new File(filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(myFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XSSFWorkbook myWorkBook = null;
		try {
			myWorkBook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);

		String refCelda = "";
		HashMap<String, String> fila = new HashMap<String, String>();
		int numColumna = 0, numFila = 0;

		if (numeroFila > mySheet.getLastRowNum()) {
			System.out.println("El n�mero de fila es inexistente para el archivo indicado!");
			System.exit(0);
		}
		Row tmp = mySheet.getRow(numeroFila - 1);
		System.out.println("------ Usando datos de la fila " + (numeroFila + 1) + "------");
		for (int col = 0; col < MAXIMO_COLUMNAS; col++) {
			numColumna = col + 1;
			refCelda = CellReference.convertNumToColString(col) + (numeroFila);
			if (tmp.getCell(col) == null) {
				String datoNull = "";
				fila.put(refCelda, datoNull);
			} else {
				switch (tmp.getCell(col).getCellType()) {
				case Cell.CELL_TYPE_STRING:
					String datoString = tmp.getCell(col).getStringCellValue();
					fila.put(refCelda, datoString);
					break;
				case Cell.CELL_TYPE_NUMERIC:
					int datoInt = (int) tmp.getCell(col).getNumericCellValue();
					fila.put(refCelda, String.valueOf(datoInt));
					break;
				case Cell.CELL_TYPE_BOOLEAN:
					String datoBoolean = "[celda booleana]";
					fila.put(refCelda, datoBoolean);
					break;
				case Cell.CELL_TYPE_BLANK:
					String datoBlank = "";
					fila.put(refCelda, datoBlank);
					break;
				default:
					fila.put(refCelda, String.valueOf(tmp.getCell(col)));

				}
			}
		} // del for de columnas

		return fila;
	} // fin del m�todo leerExcel

	/*
	 * Este m�todo imprime el contenido de las celdas de un excel pasado como
	 * par�metro. (excel con extensi�n .XLSX)
	 */
	@SuppressWarnings({ "unused", "deprecation" })
	public void leerExcel(String archivo) {
		String filename = archivo;

		File myFile = new File(filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(myFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XSSFWorkbook myWorkBook = null;
		try {
			myWorkBook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);

		String refCelda = "";
		HashMap<String, String> hoja = new HashMap<String, String>();
		int numColumna = 0, numFila = 0;

		for (int f = 0; f < MAXIMO_FILAS; f++) {
			numFila = f + 1;
			org.apache.poi.ss.usermodel.Row tmp = mySheet.getRow(f);
			System.out.println("--------------------- Datos de la fila " + (f + 1) + "---------------------");
			for (int col = 0; col < MAXIMO_COLUMNAS; col++) {
				numColumna = col + 1;
				refCelda = CellReference.convertNumToColString(col) + (f + 1);
				if (tmp.getCell(col) == null) {
					String datoNull = "";
					System.out.println(
							" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna + ") = " + datoNull);
				} else {
					switch (tmp.getCell(col).getCellType()) {
					case Cell.CELL_TYPE_STRING:
						String datoString = tmp.getCell(col).getStringCellValue();
						System.out.println(" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna
								+ ") = " + datoString);
						break;
					case Cell.CELL_TYPE_NUMERIC:
						int datoInt = (int) tmp.getCell(col).getNumericCellValue();
						System.out.println(" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna
								+ ") = " + datoInt);
						break;
					case Cell.CELL_TYPE_BOOLEAN:
						String datoBoolean = "[celda booleana]";
						System.out.println(" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna
								+ ") = " + datoBoolean);
						break;
					case Cell.CELL_TYPE_BLANK:
						String datoBlank = "";
						System.out.println(" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna
								+ ") = " + datoBlank);
						break;
					default:
						System.out.println(" Celda " + refCelda + " (Fila " + (f + 1) + ", Columna " + numColumna
								+ ") = " + String.valueOf(tmp.getCell(col)));
					}
				}
				hoja.put(refCelda, String.valueOf(tmp.getCell(col)));
			} // del for de columnas
		} // del for de filas
	} // fin del m�todo leerExcel

	
	public static int maximofilas(String archivo) {
		String filename = archivo;
		int maximo = 0;
		File myFile = new File(filename);
		FileInputStream fis = null;
		try {
			fis = new FileInputStream(myFile);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		XSSFWorkbook myWorkBook = null;
		try {
			myWorkBook = new XSSFWorkbook(fis);
		} catch (IOException e) {
			e.printStackTrace();
		}
		XSSFSheet mySheet = myWorkBook.getSheetAt(0);

		   maximo = mySheet.getLastRowNum()+1;
			 System.out.println(maximo);
			return maximo;
	
	} // fin del m�todo leerExcel
	
	
}